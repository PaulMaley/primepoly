module PrimePoly(primePoly) where


primePoly :: (Int,Int,Int,Int,Int,Int,Int,Int,Int,Int,Int,Int,Int,Int,Int,Int,
              Int,Int,Int,Int,Int,Int,Int,Int,Int,Int) -> Int
primePoly (a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z) =
  (k+2)*(
   1 - (w*z+h+j-q)^2
   - ((g*k+2*g+k+1) * (h+j)+h-z)^2
   - (2*n+p+q+z-e)^2
   - (16*(k+1)^3*(k+2)*(n+1)^2+1-f^2)^2
   - (e^3*(e+2)*(a+1)^2+1-o^2)^2
   - ((a^2-1)*y^2+1-x^2)^2
   - (16*r^2*y^4*(a^2-1)+1-u^2)^2
   - (((a+u^2*(u^2-a))^2-1)*(n+4*d*y)^2+1-(x+c*u)^2)^2
   - (n+l+v-y)^2
   - ((a^2-1)*l^2+1-m^2)^2
   - (a*i+k+1-l-i)^2
   - (p+l*(a-n-1)+b*(2*a*n+2*a-n^2-2*n-2)-m)^2
   - (q+y*(a-p-1)+s*(2*a*p+2*a-p^2-2*p-2)-x)^2
   - (z+p*l*(a-p)+t*(2*a*p-p^2-1)-p*m)^2
  )
